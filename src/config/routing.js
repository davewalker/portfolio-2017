import React, { Component } from 'react';
import Scroll from 'react-scroll';
import { withRouter, Switch, Route } from 'react-router-dom';
import { Home, About, Blog, BlogPost, Contact, Skills, Error404 } from '../pages';
import ReactGA from 'react-ga';

class Routing extends Component {

    constructor(props) {
        super(props);
        this.scroll = Scroll.animateScroll;
        this.state = {
            transitionSpeed: 350
        };
    }

    componentDidUpdate(prevProps) {
        this._trackPageview();

        if (this.props.location.pathname !== prevProps.location.pathname) {
            this._resetScroll();
        }
    }

    componentDidMount() {
        this._trackPageview();
    }

    _trackPageview() {
        ReactGA.initialize("UA-24617737-1");
        ReactGA.pageview(this.props.location.pathname);
    }

    _resetScroll() {
        this.scroll.scrollToTop({ duration: this.state.transitionSpeed });
    }

    render() {
        return (
            <Switch>
                <Route exact path="/">
                    <Home {...this.props} />
                </Route>
                <Route path="/what-i-do">
                    <Skills {...this.props} />
                </Route>
                <Route path="/about">
                    <About {...this.props} />
                </Route>
                <Route exact path="/blog">
                    <Blog {...this.props} />
                </Route>
                <Route exact path="/blog/:slug" render={(props) => <BlogPost doctitle={this.props.doctitle} {...props} />} />
                <Route path="/hello">
                    <Contact {...this.props} />
                </Route>
                <Route status={404}>
                    <Error404 {...this.props} />
                </Route>
            </Switch>
        );
    }


};

export default withRouter(Routing);
