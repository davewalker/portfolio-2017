import React, { Component } from 'react';
import { LazyImage } from './Global';

class ProfilePicture extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imgAlt: "David Walker building HTML in West Yorkshire",
            image: "/assets/img/profile/1"
        };
    }

    _getImage(format = '.jpg') {
        return this.state.image + format;
    }

    render() {
        return (
            <LazyImage src={this._getImage()} alt={this.state.imgAlt} width="709" height="475" />
        );
    }

}

export default ProfilePicture;
