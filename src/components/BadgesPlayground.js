import React, { Component } from 'react';
import Matter from 'matter-js';
import decomp from 'poly-decomp';
// import 'pathseg';

window.decomp = decomp;

class BadgesPlayground extends Component {

    constructor(props) {
        super(props);

        this.state = {
            badges: []
        };
    }

    componentDidMount() {
        this.fetchBadges();
        setTimeout(() => {
            this.initMatterAnimation();
        }, 500);
    }

    getRandomPosition() {
        const min = 400, max = 500;
        return Math.random() * (max - min) + min;
    }

    fetchBadges() {
        const data = require('./api/badge-data.json');
        this.setState({badges: data});
    }

    doPreloading() {
        Object.keys(this.state.badges).forEach((badge) => {
            let image = new Image();
            image.src = `/assets/svg/${badge}.svg`;
        });
    }

    storeSVGVertexSets() {
        let itemsProcessed = 0;
        let badgeData = {};

        const badges = [
            '10',
            'apple',
            'coffee',
            'cons',
            'cats',
            'diving',
            'ipa',
            'koala',
            'skydives',
            'whale',
            'yorkshire'
        ];

        badges.forEach((badge, index) => {

            fetch(`../assets/svg/${badge}-outline.svg`)
                .then((response) => {
                    return response.text();
                }).then((text) => {
                    const parser = new DOMParser();
                    const _svg = parser.parseFromString(text, 'image/svg+xml');
                    let paths = _svg.getElementsByTagName('path');

                    let vertexSets = this.convertSvgPathData(paths);
                    badgeData[badge] = vertexSets;

                    itemsProcessed++;

                    if (itemsProcessed === badges.length) {
                      console.log(JSON.stringify(badgeData));
                    }
                });
        });
    }

    initMatterAnimation() {
        // this.storeSVGVertexSets();
        // return;

        var Engine = Matter.Engine,
            Render = Matter.Render,
            Runner = Matter.Runner,
            Composite = Matter.Composite,
            // Common = Matter.Common,
            MouseConstraint = Matter.MouseConstraint,
            Mouse = Matter.Mouse,
            World = Matter.World,
            Bodies = Matter.Bodies;

        // create engine
        var engine = Engine.create(),
            world = engine.world;

        // create renderer
        var render = Render.create({
            element: document.querySelector('.badge-tank__container'),
            engine: engine,
            options: {
                showAngleIndicator: false,
                background: 'transparent',
                wireframes: false,
                height: 1600,
                width: 1600,
                hasBounds: false,
            }
        });

        Render.run(render);

        // create runner
        var runner = Runner.create();
        Runner.run(runner, engine);

        // add bodies
        var offset = 25,
            options = {
                isStatic: true
            };

        World.add(world, [
            //Bodies.rectangle(400, -offset, 800.5 + 2 * offset, 50.5, options),
            Bodies.rectangle(400, 700 + offset, 800.5 + 2 * offset, 50.5, options),
            Bodies.rectangle(800 + offset, 400, 50.5, 600.5 + 2 * offset, options),
            Bodies.rectangle(-offset, 400, 50.5, 600.5 + 2 * offset, options)
        ]);

        // Loop through the badges collected from the JSON file
        Object.keys(this.state.badges).forEach((icon) => {
            const vertexPoints = this.state.badges[icon];

            World.add(world, Bodies.fromVertices(this.getRandomPosition(),  0 - (100 * 1), vertexPoints, {
                restitution: 0.5,
                friction: 0.5,
                density: 0.5,
                isStatic: false,
                render: {
                    //fillStyle: 'transparent',
                    //strokeStyle: 'transparent',
                    // lineWidth: 0,
                    sprite: {
                        texture: `./assets/svg/${icon}.svg`,
                        xScale: 0.825,
                        yScale: 0.825
                    }
                }
            }, false));

        });

        // add mouse control
        var mouse = Mouse.create(render.canvas),
            mouseConstraint = MouseConstraint.create(engine, {
                mouse: mouse,
                constraint: {
                    stiffness: 0.2,
                    render: {
                        visible: false
                    }
                }
            });
        
            // Fixes scroll issues
            mouseConstraint.mouse.element.removeEventListener("mousewheel", mouseConstraint.mouse.mousewheel);
            mouseConstraint.mouse.element.removeEventListener("DOMMouseScroll", mouseConstraint.mouse.mousewheel);
        
        World.add(world, mouseConstraint);
        
        // keep the mouse in sync with rendering
        render.mouse = mouse;

        // fit the render viewport to the scene
        Render.lookAt(render, {
            min: { x: 0, y: 0 },
            max: { x: 800, y: 600 }
        });

        // wrapping using matter-wrap plugin
        var allBodies = Composite.allBodies(world);

        for (var i = 0; i < allBodies.length; i += 1) {
            allBodies[i].plugin.wrap = {
                min: { x: render.bounds.min.x - 100, y: render.bounds.min.y },
                max: { x: render.bounds.max.x + 100, y: render.bounds.max.y }
            };
        }

        // context for MatterTools.Demo
        return {
            engine: engine,
            runner: runner,
            render: render,
            canvas: render.canvas,
            stop: function() {
                Matter.Render.stop(render);
                Matter.Runner.stop(runner);
            }
        };

    }

    convertSvgPathData(paths) {
        const Svg = Matter.Svg,
            Vertices = Matter.Vertices,
            vertexSets = [];

        // Get the vertices from the SVG
        for (var i = 0; i < paths.length; i++) {
            var points = Svg.pathToVertices(paths[i], 15);
            vertexSets.push(Vertices.scale(points, 0.85, 0.85));
        }

        return vertexSets;
    }

    render() {
        let badges = [];
        Object.keys(this.state.badges).forEach((badge, i) => {
            badges.push(
                <img key={`badge-${i}`} src={`./assets/svg/${badge}.svg`} alt={`${badge} badge`} />
            );
        });

        return (
            <div className="badge-tank">
                <div className="badge-tank__container">
                </div>

                <div className="u-hidden">
                    {badges}
                </div>
            </div>
        );
    }

}

export default BadgesPlayground;
