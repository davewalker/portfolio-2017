import React, { Component } from 'react';

function ScrollHint(props) {
    if (props.showHint) {
        return (
            <div className="hero__scroll-hint">Scroll</div>
        );
    }

    return null;
}

class Hero extends Component {
    constructor(props) {
        super(props);

        this.props = props;
        this.state = {
            isLoaded: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ isLoaded: true });
        }, 0)
    }

    render() {
        const { isShort } = this.props
        return (
            <div className={`hero ${isShort ? 'hero--short' : ''}`} data-is-loaded={this.state.isLoaded}>
                <div className="hero__inner">
                    {this.props.children}
                </div>
                <ScrollHint showHint={this.props.showHint} />
            </div>
        );
    }

}

Hero.defaultProps = {
    isShort: false
}

export default Hero;
