const BREAKPOINTS = {
    phone: 320,
    tablet: 768,
    desktop: 1100
};

class Screen {

    constructor() {
        this.breakpoints = BREAKPOINTS;
        this.window = window;
        this.screenWidth = this.getScreenWidth();
        this.onResizeEvent();
    }

    setWindowSize() {
        this.screenWidth = this.getScreenWidth();
    }

    getScreenWidth() {
        return this.screenWidth;
    }

    onResizeEvent() {
        this.window.onresize = (e) => {
            this.setWindowSize();
        };
    }

    isMobile() {
        return this.getScreenWidth() < this.breakpoints.tablet;
    }


}

export default new Screen();
