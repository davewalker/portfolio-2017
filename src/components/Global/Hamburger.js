import React, { Component } from 'react';

class Hamburger extends Component {
    render() {
        return (
            <div {...this.props} className="hamburger">
                <span className="hamburger__filling"></span>
            </div>
        );
    }
}

export default Hamburger;
