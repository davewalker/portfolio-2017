import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Logo extends Component {
    render() {
        return (
            <div {...this.props} className="header__logo">
                <Link to="/">
                    <img src="/assets/img/logo.svg" alt="David Walker logo" />
                </Link>
            </div>
        );
    }
}

export default Logo;
