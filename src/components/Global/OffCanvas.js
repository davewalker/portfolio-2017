import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class OffCanvas extends Component {

    constructor(props) {
        super(props);
        this.navigateHandler = this.navigateHandler.bind(this);
    }

    navigateHandler() {
        this.props.onChange();
    }

    render() {
        return (
            <div className="off-canvas" {...this.props}>
                <div className="off-canvas__inner">
                    <nav className="navigation">
                        <ul className="navigation__list">
                            <li data-num="01" className="navigation__list-item">
                                <NavLink onClick={this.navigateHandler} exact className="navigation__link" activeClassName="navigation__link--active" to="/">Home</NavLink>
                            </li>
                            <li data-num="02" className="navigation__list-item">
                                <NavLink onClick={this.navigateHandler} className="navigation__link" activeClassName="navigation__link--active" to="/what-i-do">What I do</NavLink>
                            </li>
                            <li data-num="03" className="navigation__list-item">
                                <NavLink onClick={this.navigateHandler} className="navigation__link" activeClassName="navigation__link--active" to="/blog">Blog</NavLink>
                            </li>
                            <li data-num="04" className="navigation__list-item">
                                <NavLink onClick={this.navigateHandler} className="navigation__link" activeClassName="navigation__link--active" to="/about">About</NavLink>
                            </li>
                            <li data-num="05" className="navigation__list-item">
                                <NavLink onClick={this.navigateHandler} className="navigation__link" activeClassName="navigation__link--active" to="/hello">Say hello</NavLink>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        );
    }
}

export default OffCanvas;
