import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.date = new Date();
        this.telephone_number = "07702010144";
    }

    render() {
        return (
            <footer className="footer">
                <div className="footer__inner">
                    <div className="footer__block gamma">
                        { "Have you got a project you'd like to discuss? " }
                        <div className="footer__link">
                            <Link className="no-break" to="/hello">Let's talk</Link>
                        </div>
                    </div>

                    <div className="footer__block gamma">{ "Or give me a call on " + this.telephone_number }</div>
                    <h6 className="epsilon">{ "\u00A9 David Walker " + this.date.getFullYear() }</h6>

                    <div className="footer__logos-container u-text-center">
                        <a className="footer__logo u-unstyled" href="https://buttercms.com" title="Powered by ButterCMS" target="_blank" rel="noopener noreferrer">
                            <img width="140" src="https://cdn.buttercms.com/PGJPyIwaQ2KnOA8UyKfH" alt="ButterCMS logo" />
                        </a>
                        <a className="footer__logo u-unstyled" href="https://www.sas.org.uk/" title="Proud to support Surfers Against Sewage 2019-2020" target="_blank" rel="noopener noreferrer">
                            <img width="85" src="/assets/img/footer/250Club_logo 2019-2020-1.jpg" alt="Surfers Against Sewage Partner logo" />
                        </a>
                    </div>
                </div>
            </footer>
        );
    }

}
export default Footer;
