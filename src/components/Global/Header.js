import React, { Component } from 'react';
import Headroom from 'react-headroom';
import Scroll from 'react-scroll'; // Imports all Mixins

import Logo from './Logo';
import Hamburger from './Hamburger';
import OffCanvas from './OffCanvas';

class Header extends Component {
    constructor(props) {
        super(props);

        this.removeClass = this.removeClass.bind(this);
        this.toggleClass = this.toggleClass.bind(this);
        this.onNavigate = this.onNavigate.bind(this);
        this.scroll = Scroll.animateScroll;
        this.scrollPosition = 0;

        this.state = {
            menuOpen: false,
            transitionSpeed: 350,
            scrollPosition: 0
        };
    }

    componentDidMount() {
        // add classname to initialise JS
        document.body.classList.add('is-loaded');

        // setTimeout to remove the preloader element
        const preloader = document.getElementsByClassName('preloader')[0];
        setTimeout(() => {
            preloader.parentNode.removeChild(preloader);
        }, 1000);
    }

    resetScroll() {
        this.scroll.scrollToTop({ duration: this.state.transitionSpeed });
    }

    onNavigate() {
        setTimeout(() => {
            this.toggleClass();
        }, 500);
    }

    removeClass() {
        this.setState({ menuOpen: false });
        document.body.classList.remove('no-scroll');
        this.resetScroll();
    }

    toggleClass() {
        const currentState = this.state.menuOpen,
              html = document.getElementsByTagName( 'html' )[0];
        this.setState({
            menuOpen: !currentState
         });

        setTimeout(() => {
            html.classList.toggle('no-scroll');
        }, this.state.transitionSpeed);
    }

    render() {
        return (
            <div>
                <Headroom>
                    <header className="header">
                        <OffCanvas {...this.props} data-is-open={this.state.menuOpen} onChange={this.onNavigate} />
                        <Logo onClick={this.removeClass} />
                        <Hamburger onClick={this.toggleClass} data-is-open={this.state.menuOpen} />
                    </header>
                </Headroom>
            </div>
        );
    }
}

export default Header;
