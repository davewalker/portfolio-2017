import React, { Component } from 'react';

class LazyImage extends Component {

    constructor(props) {
        super(props);
        this.config = {
            rootMargin: '100px'
        };
    }

    componentDidMount() {        
        // If we support Intersection Observer, set it up
        // Otherwise, let's just load all in the images
        if (this.supportsIO()) {
            this.observe(this.refs.image);
            this.observer.observe(this.refs.image);
        }
        else {
            this.loadImage();
        }
    }

    supportsIO() {
        return 'IntersectionObserver' in window;
    }

    observe() {
        this.observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {      
                    this.loadImage();
                }
            });
        }, this.config);
    }

    loadImage() {
        const loadedImage = new Image();
        loadedImage.src = this.refs.image.dataset.lazy;
        loadedImage.newImage = this.refs.image;
        loadedImage.placeholderImage = this.refs.imagePlaceholder;
        loadedImage.onload = function() {
            loadedImage.newImage.src = this.src;
            setTimeout(() => {
                loadedImage.placeholderImage.classList.add('u-placeholder-img--is-loaded');
            }, 50);
        }
    }

    render() {
        return (
            <div className="u-placeholder-img" ref="imagePlaceholder">
                <img ref="image" data-lazy={this.props.src} width={this.props.width} height={this.props.height} alt={this.props.alt} />
            </div>
        );
    }

}

export default LazyImage;