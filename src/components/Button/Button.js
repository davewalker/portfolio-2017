import React, { Component } from 'react';

class Button extends Component {
    render() {
        return (
            <div className="button">
                <a href={this.props.link}>{this.props.label}</a>
            </div>
        );
    }
}

export default Button;
