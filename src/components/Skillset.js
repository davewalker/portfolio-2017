import React, { Component } from 'react';
import Signpost from '../components/Signpost';

class Skillset extends Component {

    constructor(props) {
        super(props);

        this.state = {
            skills: []
        };
    }

    componentDidMount() {
        const json = require('./api/skills-data.json');
        this.setState({
            skills: json
        });
    }

    render() {

        const skills = [];
        this.state.skills.forEach((skill, index) => {

            skills.push(
                <Signpost key={skill.key} className="c-signpost c-signpost--alternating c-signpost--small" isActive="true">
                    <div className="c-signpost__panel c-signpost__panel--with-text">
                        <div className="c-signpost__overlay-content u-padding">
                            <h3 className="u-secondary-color">{skill.title}</h3>
                            <div dangerouslySetInnerHTML={{__html: skill.content}}></div>
                        </div>
                    </div>
                </Signpost>
            );

        });


        return (
            <div className="c-skill-list">
                {skills}
            </div>
        );
    }

}

export default Skillset;
