import React, { Component } from 'react';

class ProjectListNavigation extends Component {

    navigateHandler(obj, e) {
        this.props.clickHandler(obj);
    }

    render() {
        var links = [];

        if (Object.keys(this.props.projects).length) {
            this.props.projects.forEach((project) => {
                links.push(
                    <li key={project.key} className="c-slideshow__nav-list-item" data-selected={this.props.currentTab === project.key}>
                        <a onClick={() => this.navigateHandler('#' + project.key)} className="c-slideshow__nav-link u-unstyled" href={"#" + project.key}>
                            {project.title}
                        </a>
                    </li>
                );
            });
        }

        return (
            <ul className="c-slideshow__nav-list">
                {links}
            </ul>
        )
    }
}

export default ProjectListNavigation;
