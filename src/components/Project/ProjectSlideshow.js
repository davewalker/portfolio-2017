import React, { Component } from 'react';
import Signpost from '../Signpost';

class ProjectSlideshow extends Component {

    constructor(props) {
        super(props);
        this.images = {};

        this.config = {
            rootMargin: '100px'
        }
        this.state = {
            x: 0,
            y: 0
        };
    }

    _lazyLoadImages() {
        if (this.props.projects.length) {
            this.props.projects.forEach((project) => {
                const imageRef = 'project-image__' + project.key,
                      smallImageRef = 'project-small-image__' + project.key;
                            
                this.refs[smallImageRef].srcset = this.refs[smallImageRef].dataset.lazy;
                this.refs[imageRef].src = this.refs[imageRef].dataset.lazy;
            });
        }
    }

    _supportsIO() {
        return 'IntersectionObserver' in window;
    }

    _observe() {
        this.observer = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {      
                    this._lazyLoadImages();
                }
            });
        }, this.config);
    }

    componentDidMount() {
        if (this._supportsIO()) {
            this._observe(this.refs.slideshow);
            this.observer.observe(this.refs.slideshow);
        }
        else {
            this._lazyLoadImages();
        }
    }

    render() {
        var slides = [];
        if (this.props.projects.length) {
            this.props.projects.forEach((project) => {

                // Loop through bulletpoint data
                const bullets = [];
                project.bullets.forEach((bullet, i) => {
                    bullets.push(
                        <li key={i}>{bullet}</li>
                    );
                });

                slides.push(
                    <Signpost className="c-signpost c-signpost--project" key={project.key} isActive={this.props.currentTab === project.key}>
                        <div className="c-signpost__columns">
                            <div className="c-signpost__column c-signpost__image">
                                <picture>
                                    <source ref={"project-small-image__" + project.key} media="(max-width:800px)" data-lazy={project.smallImage} />
                                    <img ref={"project-image__" + project.key} data-lazy={project.image} alt={project.title} width="710" height="500" />
                                </picture>                                
                            </div>

                            <div className="c-signpost__column c-signpost__column--single">
                                <div className="c-signpost__overlay-content u-padding">
                                    <h3 className="beta u-secondary-color u-margin--bottom-half">{project.title}</h3>
                                    <ul className="c-signpost__list">
                                        {bullets}
                                    </ul>
                                    <a href={project.url} rel="noopener" className="c-signpost__link u-margin--top u-external-link u-unstyled">View project</a>
                                </div>
                            </div>
                        </div>
                    </Signpost>
                );
            });
        }

        return (
            <div ref="slideshow" className="c-project-slideshow">
                {slides}
            </div>
        )

    }

}

export default ProjectSlideshow;
