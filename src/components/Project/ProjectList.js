import React, { Component } from 'react';

import ProjectListNavigation from './ProjectListNavigation';
import ProjectSlideshow from './ProjectSlideshow';

class ProjectList extends Component {

    constructor(props) {
        super(props);
        this.navigateHandler = this.navigateHandler.bind(this);

        this.state = {
            currentTab: window.location.hash ? window.location.hash.replace('#', '') : '',
            projects: {}
        };
    }

    componentDidMount() {
        const data = require('../api/project-data.json');
        this.setState({
            projects: data,
            currentTab: this.state.currentTab || data[0]['key'] 
        });
    }

    navigateHandler(obj) {
        const target = obj.replace('#', '');
        this.setState({
            currentTab: target
        });

        return false;
    }

    render() {
        const PROJECTS = this.state.projects;
        return (
            <div className="c-slideshow">
                <nav className="c-slideshow__nav gamma">
                    <ProjectListNavigation projects={PROJECTS} currentTab={this.state.currentTab} clickHandler={this.navigateHandler} />
                </nav>
                <div className="c-slideshow__slider">
                    <ProjectSlideshow projects={PROJECTS} currentTab={this.state.currentTab} />
                </div>
            </div>
        )
    }


}

export default ProjectList;
