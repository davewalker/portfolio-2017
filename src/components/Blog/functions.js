const calculateReadTime = (content) => {
    const wordsPerMinute = 200;
    const wordCount = content.split(' ').length
    
    let mins = 1

    if (wordCount > 0) {
        mins =  Math.ceil(wordCount / wordsPerMinute)
    }

    return `${mins} min`
}


export {
    calculateReadTime
}