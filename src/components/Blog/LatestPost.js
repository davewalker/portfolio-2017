import React, { Component } from 'react'
import moment from 'moment'

import ButterCMS from '../../api/butterCMS';

import { Link } from 'react-router-dom';
import { Signpost } from '../'
import { LazyImage, LoadingSpinner } from '../Global';
import { calculateReadTime } from '../Blog/functions'

export default class LatestPost extends Component {

    state = {
        post: false
    }

    componentDidMount() {
        this.getLatestPost()
    }

    async getLatestPost() {
        const postData = await ButterCMS.post.list({ page: 1, page_size: 1 });

        this.setState({ 
            post: postData.data.data[0]
        })
    }

    render() {
        const { post } = this.state

        if (!post) {
            return <LoadingSpinner />
        }
        
        return (
            <Signpost className="c-signpost c-signpost--medium c-signpost--single c-signpost--post u-no-bg u-text-left" isActive={true}>
                <div className="c-signpost__columns">
                    <div className="c-signpost__column c-signpost__column--flexed u-margin--bottom">
                        <div className="u-padding">
                            <p className="epsilon">Latest post</p>
                            <p className="c-signpost__text">{post.title}</p>
                            <p className="u-light-color u-margin--bottom epsilon">{moment(post.published).format('LL')} // {`${calculateReadTime(post.body)} read`}</p>
                            <p>{post.summary}</p>

                            <Link className="c-signpost__link u-more-link u-unstyled" to={`/blog/${post.slug}`}>Read post</Link>
                        </div>
                    </div>
                    {post.featured_image ?
                        <div className="c-signpost__column c-signpost__image">
                            <LazyImage src={post.featured_image} alt={post.featured_image_alt} width="709" height="475" />
                        </div>
                    : ''}
                </div>
            </Signpost>
        )
    }

}