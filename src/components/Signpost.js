import React, { Component } from 'react';

class Signpost extends Component {

    render() {
        return (
            <div className={(this.props.className ? this.props.className : "c-signpost")} aria-hidden={!this.props.isActive}>
                {this.props.children}
            </div>
        );
    }

}

export default Signpost;
