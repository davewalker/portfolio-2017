import React, { Component } from 'react';

class ContentBlock extends Component {
    render() {
        return (
            <div className="content-block" {...this.props}>
                { this.props.children }
            </div>
        );
    }
}

export default ContentBlock;
