import React, { Component } from 'react';
import { Hero } from '../components';

class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            metaDescription: 'Do you have a web project you\'d like to discuss? Get in touch via email or contact me via LinkedIn and Twitter'
        }
    }

    componentDidMount() {
        document.title = "Say hello // " + this.props.doctitle;
        document.querySelector('meta[name="description"]').content = this.state.metaDescription;
    }

    render() {
        return (
            <div key="Contact">
                <Hero className="hero hero--narrow">
                    <h1 className="hero__title alpha">Say hello</h1>
                    <div className="hero__sub-content">
                        <h2 className="beta">Have you got a project you’d like to discuss? Let’s talk...</h2>
                        <ul>
                            <li className="beta"><a href="mailto:hello@davidwalker.info">hello@davidwalker.info</a></li>
                            <li className="beta"><a href="tel:07702010144">07702 01 01 44</a></li>
                            <li className="beta">
                                <a href="https://www.linkedin.com/in/davidwalker1986/">LinkedIn</a> | <a href="https://twitter.com/davwalkr">Twitter</a>
                            </li>
                        </ul>
                    </div>
                </Hero>
            </div>
        );
    }
}

export default Contact;
