import React, { Component } from 'react';
import { ContentBlock, Hero, Skillset } from '../components';

class Skills extends Component {

    constructor(props) {
        super(props);

        this.state = {
            metaDescription: 'I specialise in building responsive websites and applications using HTML, CSS & Javascript. This includes React and VueJS, as well as jQuery and AngularJS.. I\'ve worked with many open source PHP frameworks too; Wordpress, Shopify, Magento, Laravel, Symfony. I also have experience with CLI tool - Jenkins, Gulp, Grunt'
        };
    }

    componentDidMount() {
        document.title = "What I do // " + this.props.doctitle;
        document.querySelector('meta[name="description"]').content = this.state.metaDescription;
    }

    render() {
        return (
            <div key="Skills">
                <Hero showHint={ true }>
                    <h1 className="hero__title alpha">What I do</h1>
                    <div className="hero__sub-content">
                        <h2 className="beta">
                            {"As a front-end developer I love crafting website of all shapes and sizes. I use modern tech to create the best possible end-product, to the highest specification and with a real attention to detail"}
                        </h2>
                    </div>
                </Hero>

                <ContentBlock className="content-block content-block--contained">
                    <h3 className="beta u-text-center u-no-bm">{"I can create a website that'll look great on every device and maximise your brand's potential. Let me worry about what's going on under the hood, but I have huge experience with all kinds of web technology."}</h3>
                </ContentBlock>

                <ContentBlock className="content-block content-block--contained">
                    <Skillset />
                </ContentBlock>

            </div>
        )
    }
}

export default Skills;
