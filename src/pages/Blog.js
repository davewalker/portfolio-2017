import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment'
import ButterCMS from '../api/butterCMS';
import { Hero, ContentBlock } from '../components';

const calculateReadTime = (content) => {
    const wordsPerMinute = 200;
    const wordCount = content.split(' ').length
    
    let mins = 1

    if (wordCount > 0) {
        mins =  Math.ceil(wordCount / wordsPerMinute)
    }

    return `${mins} min`
}

class Blog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            metaDescription: 'Some blog posts',
            featuredArticle: false,
            heroImageLoaded: false,
            articles: []
        }

        this.onImageLoaded = this.onImageLoaded.bind(this)

    }

    componentDidMount() {
        document.title = 'Blog // ' + this.props.doctitle;
        document.querySelector('meta[name="description"]').content = this.state.metaDescription;

        this.getLatestPosts()
    }

    async getLatestPosts() {
        const posts = await ButterCMS.post.list({ page: 1, page_size: 10 });
        const featuredArticle = posts.data.data.shift()

        this.setState({
            featuredArticle,
            articles: posts.data.data,
            isLoading: false 
        })
    }

    onImageLoaded() {
        this.setState({
            heroImageLoaded: true
        })
    }

    render() {
        const { featuredArticle, heroImageLoaded, articles, isLoading } = this.state

        if (isLoading) {
            return (
                <div className="preloader preloader--force-display"></div>
            )
        }

        return (
            <div key="Blog">
                {featuredArticle ?
                    <Hero isShort={true}>
                        <img className={`hero__bg ${heroImageLoaded ? `hero__bg--loaded` : ``}`} src={featuredArticle.featured_image} alt={featuredArticle.featured_image_alt} onLoad={this.onImageLoaded} />

                        <h1 className="hero__title alpha">{featuredArticle.title}</h1>
                        <div className="hero__sub-content">
                            <h2 className="gamma">
                                {featuredArticle.summary}
                            </h2>
                            <p className="u-white-color epsilon u-margin--bottom">{moment(featuredArticle.published).format('LL')} // {`${calculateReadTime(featuredArticle.body)} read`}</p>
                                
                            <Link className="c-signpost__link c-signpost__link--inline u-more-link" to={`/blog/${featuredArticle.slug}`}>
                                Read article
                            </Link>
                        </div>
                    </Hero>
                : ''}

                <div>
                    {articles.map((article, k) => (
                        <ContentBlock className="content-block content-block--article" key={k}>
                            <h2 className="beta">
                                <Link className="u-unstyled" to={`/blog/${article.slug}`}>{article.title}</Link>
                            </h2>
                            <p className="u-margin--bottom-half">{article.summary}</p>

                            <div className="c-article__meta">
                                <div className="epsilon u-margin--bottom">
                                    <p className="u-light-color u-no-bm epsilon">{moment(article.published).format('LL')} // {`${calculateReadTime(article.body)} read`}</p>
                                </div>

                                <Link className="c-signpost__link c-signpost__link--inline u-more-link" to={`/blog/${article.slug}`}>
                                    Read article
                                </Link>
                            </div>
                        </ContentBlock>
                    ))}
                </div>

            </div>
        );
    }
}

export default Blog;
