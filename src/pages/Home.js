import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ContentBlock, Hero, LatestBlogPost, ProjectList, ProfilePicture, Signpost } from '../components';

function AvailabilityMessage(props) {
    if (props.isAvailable) {
        return (
            <span>
            I am currently available <span className="u-no-break--lrg">for work. <Link to="/hello">Let's talk</Link></span>
            </span>
        );
    }

    return (
        <span>
            I'm not currently available <span className="u-no-break--lrg">for work. <Link to="/hello">Drop me a line</Link></span>
        </span>
    );
}


class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isAvailable: true,
            metaDescription: 'Hi, I\'m David Walker, a Senior Frontend Engineer based in Leeds, Yorkshire, UK. I specialise in creating fast, responsive websites with HTML/SASS with the added magic of React and VueJS. I\'ve worked with some major brands in my time, check out my portfolio.'
        };
    }

    componentDidMount() {
        document.title = this.props.doctitle;
        document.querySelector('meta[name="description"]').content = this.state.metaDescription;
    }

    render() {
        return (
            <div key="Home">
                <Hero showHint="true">
                    <h1 className="hero__title alpha">
                        Hello. My name is <span className="u-no-break">David Walker.</span><br />
                        I'm a Senior Frontend Engineer and I build websites.
                    </h1>
                    <h2 className="hero__title alpha u-no-bm">
                        <AvailabilityMessage isAvailable={this.state.isAvailable} />
                    </h2>

                </Hero>

                <ContentBlock className="content-block content-block--contained">
                    <h3 className="beta u-text-center u-no-bm">{"I've had the thrill of collaborating with some amazing brands, here are just a few..."}</h3>
                </ContentBlock>

                <ContentBlock className="content-block">
                    <ProjectList />
                </ContentBlock>

                <ContentBlock className="content-block content-block--fluid content-block--spotted-bg u-no-padding u-text-center">

                    <ContentBlock className="content-block content-block--contained">
                        <h3 className="beta u-no-bm">{"As a frontend developer, I love crafting websites of all shapes and sizes."}</h3>
                    </ContentBlock>

                    <ContentBlock className="content-block">
                        <Signpost className="c-signpost c-signpost--single u-text-left" isActive={true}>
                            <div className="c-signpost__columns">
                                <div className="c-signpost__column c-signpost__image">
                                    <ProfilePicture />
                                </div>
                                <div className="c-signpost__column">
                                    <div className="c-signpost__overlay-content u-padding">
                                        <p className="c-signpost__text">{"I use modern tech to create the best possible end-product, to the highest specification and with a real attention to detail."}</p>
                                        <Link className="c-signpost__link u-more-link u-unstyled" to="/about">Read more</Link>
                                    </div>
                                </div>
                            </div>
                        </Signpost>
                    </ContentBlock>

                </ContentBlock>

                <div className="u-grey-bg">
                    <ContentBlock className="content-block">
                        <LatestBlogPost />
                    </ContentBlock>
                </div>

            </div>
        );
    }
}

export default Home;
