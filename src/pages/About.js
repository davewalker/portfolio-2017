import React, { Component } from 'react';
import { BadgesPlayground, Hero } from '../components';

class About extends Component {
    constructor(props) {
        super(props);

        this.state = {
            metaDescription: 'I am a freelance front and backend web developer based in Leeds, West Yorkshire. I have over 10 years industry experience both as a full time employee and as a freelancer. I\'ve worked with different sizes companies in Leeds and in Sydney, Australia (where I spent a year living and working). I love travelling, skydiving and cats.'
         };
    }

    componentDidMount() {
        document.title = 'About me // ' + this.props.doctitle;
        document.querySelector('meta[name="description"]').content = this.state.metaDescription;
    }

    render() {
        return (
            <div key="About">
                <Hero ref="heroContainer">
                    <h1 className="hero__title alpha">About</h1>
                    <div className="hero__sub-content">
                        <h2 className="beta">
                            {"I have more than 10 years experience working with websites of all shapes and sizes. I’ve worked with business across the globe having spent time living and working abroad - ranging from small, ambitious startups, digital agencies right through to large corporate organisations."}
                        </h2>
                    </div>

                    <BadgesPlayground />
                </Hero>
            </div>
        );
    }
}

export default About;
