import React, { Component } from 'react';
import { Hero } from '../components';

class Error404 extends Component {
    render() {
        return (
            <Hero showHint>
                <h1 className="hero__title alpha">
                    Error 404.
                </h1>
            </Hero>
        );
    }
}

export default Error404;
