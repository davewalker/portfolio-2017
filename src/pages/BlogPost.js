import React, { Component } from 'react'
import _ from 'lodash'
import moment from 'moment'
import butter from '../api/butterCMS'
import { Error404 } from '../pages'
import { ContentBlock } from '../components'
import { Link } from 'react-router-dom';

const calculateReadTime = (content) => {
    const wordsPerMinute = 200;
    const wordCount = content.split(' ').length
    
    let mins = 1

    if (wordCount > 0) {
        mins =  Math.ceil(wordCount / wordsPerMinute)
    }

    return `${mins} min`
}

export default class BlogPost extends Component {

    constructor(props) {
        super(props)

        this.state = {
            article: false,
            postMeta: false,
            isLoading: true
        }
    }

    componentDidMount() {
        this.getPost()
    }

    componentDidUpdate(previousProps) {
        if ( !_.isEqual( previousProps.match.params, this.props.match.params ) ) {
            this.getPost()
        }
    }

    getPost() {
        const { slug } = this.props.match.params
        
        butter.post.retrieve( slug ).then(response => {
            const article = response.data.data
            const postMeta = response.data.meta

            this.setState({ 
                article,
                postMeta,
                isLoading: false 
            })
            document.title = `${article.seo_title} // Blog // ${this.props.doctitle}`;
            document.querySelector('meta[name="description"]').content = article.meta_description;

        })
        .catch(error => {
            this.setState({isLoading: false})
        })
    }

    render() {
        const { article, postMeta, isLoading } = this.state
        const { next_post, previous_post } = postMeta

        console.log( { postMeta })

        if (isLoading) {
            return (
                <div className="preloader preloader--force-display"></div>
            )
        }
        
        if (!article) {
            return <Error404 />
        }

        return (
                <article className="c-article">
                    <div className="c-article__meta c-article__meta--dark">
                        <ContentBlock className="content-block content-block--contained">
                            <h1 className="c-article__title alpha">{article.title}</h1>
                            <h2 className="c-article__summary gamma">{article.summary}</h2>

                            <div className="c-article__author">
                                <p className="u-light-color u-no-bm epsilon">{moment(article.published).format('LL')} // {`${calculateReadTime(article.body)} read`}</p>
                            </div>
                        </ContentBlock>
                    </div>

                    {article.featured_image ?
                        <div className="c-article__featured-img">
                            <img src={article.featured_image} alt={article.featured_image_alt} />
                        </div>                    
                    : ''}

                    <ContentBlock className="c-article__content content-block content-block--contained">
                        <div dangerouslySetInnerHTML={{__html: article.body}}></div>
                    </ContentBlock>

                    <ContentBlock className="c-article__content content-block content-block--contained">
                        <div className="c-article__related-posts">
                            <div className="c-article__signpost">
                                {previous_post ?
                                    <div>
                                        <strong>Previous Article /</strong>
                                        <Link to={`/blog/${previous_post.slug}`}>
                                            {previous_post.title}
                                        </Link>    
                                    </div>
                                : ''}
                            </div>
                                
                            <div className="c-article__signpost">
                                {next_post ?
                                    <div>
                                        <strong>Next Article /</strong>
                                        <Link to={`/blog/${next_post.slug}`}>
                                            {next_post.title}
                                        </Link>   
                                    </div>
                                : ''}
                            </div>

                        </div>

                        <hr />

                        <div className="c-signpost u-margin--top-double u-text-center">
                            <Link className="c-signpost__link c-signpost__link--inline gamma" to="/blog">Back to blog</Link>
                        </div>
                        
                    </ContentBlock>

                </article>
        )
    }
}