import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routing from './config/routing';
import { Header, Footer } from './components/Global';

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div className="page-container">
                    <Header />
                    <div className="page">
                        <Routing {...this.props} />
                        <Footer />
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
