/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
    <App doctitle="David Walker // Freelance Frontend Developer based in Leeds" />
), document.getElementById('root'));


registerServiceWorker();
