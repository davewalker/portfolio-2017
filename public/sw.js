var CACHE_NAME = 'davidwalker:1100';
var urlsToCache = [
    '/',
    '/about/',
    '/what-i-do/',
    '/hello/',
    '/blog/',
    '/assets/img/logo.svg?v=' + CACHE_NAME
];

self.addEventListener('install', function(event) {
  self.skipWaiting();

  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        // console.log('Opened cache - public');
        return cache.addAll(urlsToCache);
      })
      .catch(function(err) {
        //   console.log('Error: ' + err);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});

// Empty out any caches that don�t match the ones listed.
self.addEventListener('activate', function(event) {
    var cacheWhitelist = ['davidwalker:1100'];

    event.waitUntil(
      caches.keys().then(function(cacheNames) {
        return Promise.all(
          cacheNames.map(function(cacheName) {
            if (cacheWhitelist.indexOf(cacheName) === -1) {
              return caches.delete(cacheName);
            }
          })
        );
      })
    );
  });